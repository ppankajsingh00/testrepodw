﻿CREATE TABLE [dbo].[DimDepartmentGroup] (
    [DepartmentGroupKey]       INT           NOT NULL,
    [ParentDepartmentGroupKey] INT           NULL,
    [DepartmentGroupName]      NVARCHAR (50) NULL
);

